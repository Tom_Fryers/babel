Babel
=====

Running the Game
----------------
To play Babel, simply enter `python run_game.py` into the terminal, while in
this directory. Instructions for how to play are availably in the game, or in
src/instructions.txt if you'd rather read them separately.

Dependencies
------------
* Python 3.6+
* Pygame 1.9.4

Credits
-------
I, LeopardShark, wrote all of the code for the game, and created all of the
artwork. The font for the "Babel" logo on the main screen is Noto Serif, and
the font for the rest of the game is Noto Sans. Both are available under the
SIL Open Font License 1.1 (which is included four times in the data/fonts
directory) [here](https://www.google.com/get/noto/).

All sound effects are from freesound.org:

* [Mistake](https://freesound.org/people/wertstahl/sounds/409269/)
  Licensed under [Creative Commons: Attribution 3.0 Unported License](https://creativecommons.org/licenses/by/3.0/)

* [Thud](https://freesound.org/people/OtisJames/sounds/215162/)
  Licensed under CC0.
  (I slightly trimmed off the start and reduced it to mono)

* [Click](https://freesound.org/people/lebaston100/sounds/192271/)
  Licensed under [Creative Commons: Attribution 3.0 Unported License](https://creativecommons.org/licenses/by/3.0/)

The music is "Celtic Impulse" by Kevin MacLeod (incompetech.com)

Licensed under [Creative Commons: By Attribution 3.0 License](http://creativecommons.org/licenses/by/3.0/)

I transcoded all sounds to OGG/Vorbis from their original formats.

This game was created for [Pyweek](https://pyweek.org/), a fun competition in
which entrants create a game in a single week, using Python.

Miscellaneous Information
-------------------------
The game is quite hard, unless you are a very versatile linguist: I would
recommend choosing easy mode for your first game. Try to save up lives for the
later levels in other game modes.

I do not speak any of the languages featured in the game fluently except
English, so I am unable to verify the quality of the translations. They were
all translated largely by software, so I apologise if there are mistakes or
poor sentence structures.
