import pygame
from pygame.locals import *

from . import ui, game, render, colours

def check_buttons(buttons):
    """Update a set of buttons, and get any clicks"""
    mp = pygame.mouse.get_pos()
    for button in buttons:
        button.mouse_over(mp)

    for event in pygame.event.get():
        if event.type == QUIT:
            return True
        elif event.type == MOUSEBUTTONDOWN:
            for button in buttons:
                button.click(mp)

        elif event.type == MOUSEBUTTONUP:
            for button in buttons:
                if button.release(mp):
                    return button.text


def main():
    pygame.mixer.pre_init(44100, -16, 2, 1024)
    pygame.init()
    pygame.mixer.quit()
    pygame.mixer.init(44100, -16, 2, 1024)

    pygame.mixer.music.load("data/sound/celtic-impulse.ogg")
    pygame.mixer.music.set_volume(0.4)
    pygame.mixer.music.play(-1)

    WIDTH = 1280
    HEIGHT = 720

    S = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption("Babel")

    with open("data/instructions.txt", "r") as f:
        INSTRUCTIONSTEXT = f.read().replace("\n\n", "$").replace("\n", " ").replace("$", "\n\n      ")

    # Set up menu fonts
    FONTFQ = pygame.font.Font("data/fonts/NotoSans-hinted/NotoSans-Regular.ttf", 80)
    FONTFM = pygame.font.Font("data/fonts/NotoSans-hinted/NotoSans-Regular.ttf", 60)
    
    # Set up positions for difficulty select screen
    TT = 200
    TB = 580
    
    LINEPOSES = ((( 190, TT), (1090, TT)),
                 (( 190, TB), (1090, TB)),

                 (( 190, TT), ( 190, TB)),
                 (( 490, TT), ( 490, TB)),
                 (( 790, TT), ( 790, TB)),
                 ((1090, TT), (1090, TB)),
                 )
    
    DIFFBUTLOCS = [190 + 300 * i + (300 - FONTFM.size(t)[0]) / 2 for i, t in enumerate(("Easy", "Medium", "Hard"))]

    # Create text images for menus
    INSTRUCTIONSIM = render.draw_text(INSTRUCTIONSTEXT, ("", "ttf"), boxsize=(880, 720), fontsize=30)
    CHOOSEDIFF = FONTFQ.render("Choose Difficulty", 1, colours.FOREGROUND)
    INSTR = FONTFQ.render("Instructions", 1, colours.FOREGROUND)
    OPTIONS = FONTFQ.render("Options", 1, colours.FOREGROUND)

    DIFFS = ["Lots of time and lives; key words are highlighted. Uses English, Spanish, French, Italian, German and Portuguese.",
             "Some time and lives; key words are highlighted. Uses English, Spanish, French, German, Russian and Chinese.",
             "Limited time and lives; no highlighting. Uses English, Italian, Russian, Hindi, Chinese and Bengali.",
            ]

    DIFFS = [render.draw_text(t, ("", "ttf"), boxsize=(260, 720), fontsize=30) for t in DIFFS]

    TITLE = pygame.image.load("data/logo.png")
    buttons = [ui.TextButton("Play", FONTFM, colours.TBCOLS, (200, 300)),
               ui.TextButton("Instructions", FONTFM, colours.TBCOLS, (200, 380)),
               ui.TextButton("Options", FONTFM, colours.TBCOLS, (200, 460)),
               ui.TextButton("Quit", FONTFM, colours.TBCOLS, (200, 540))]

    paused = False

    ext = False
    while not ext:
        action = check_buttons(buttons)
        if action == True or action == "Quit":
            ext = True

        elif action == "Instructions":
            backbutton = [ui.TextButton("Back", FONTFM, colours.TBCOLS, (100, 60)),]
            back = False
            while not ext or back:
                action = check_buttons(backbutton)
                if action == True:
                    ext = True
                    break

                if action:
                    if action == "Back":
                        back = True
                        break

                S.fill(colours.BACKGROUND)
                S.blit(INSTR, ((WIDTH - INSTR.get_width()) / 2, 50))
                S.blit(INSTRUCTIONSIM, (200, 180))
                for button in backbutton:
                    button.draw(S)

                pygame.display.update()

        elif action == "Options":
            optionbuttons = [ui.TextButton("Back", FONTFM, colours.TBCOLS, (100, 60)),
                             ui.TextButton("Music", FONTFM, colours.TBCOLS, (200, 300)),
                    ]
            back = False
            while not ext or back:
                action = check_buttons(optionbuttons)
                if action == True:
                    ext = True
                    break

                if action:
                    if action == "Back":
                        back = True
                        break
                    if action == "Music":
                        if paused:
                            pygame.mixer.music.unpause()
                            paused = False
                        else:
                            pygame.mixer.music.pause()
                            paused = True

                S.fill(colours.BACKGROUND)
                S.blit(OPTIONS, ((WIDTH - OPTIONS.get_width()) / 2, 50))
                
                for button in optionbuttons:
                    button.draw(S)

                pygame.display.update()


        elif action == "Play":
            difficultybuttons = [ui.TextButton("Easy", FONTFM, colours.TBCOLS, (DIFFBUTLOCS[0], 200)),
                                 ui.TextButton("Medium", FONTFM, colours.TBCOLS, (DIFFBUTLOCS[1], 200)),
                                 ui.TextButton("Hard", FONTFM, colours.TBCOLS, (DIFFBUTLOCS[2], 200)),
                                 ui.TextButton("Back", FONTFM, colours.TBCOLS, (100, 60)),]
            back = False
            while not ext or back:
                action = check_buttons(difficultybuttons)
                if action == True:
                    ext = True
                    break

                if action:
                    if action == "Back":
                        back = True
                        break
                    else:
                        result = game.play(S, action.lower())
                        if result == "quit":
                            ext = True
                        break

                S.fill(colours.BACKGROUND)
                S.blit(CHOOSEDIFF, ((WIDTH - CHOOSEDIFF.get_width()) / 2, 50))

                for linepos in LINEPOSES:
                    pygame.draw.line(S, colours.FOREGROUND, linepos[0], linepos[1], 4)

                for i in range(3):
                    S.blit(DIFFS[i], (210 + 300 * i, 280))
                for button in difficultybuttons:
                    button.draw(S)

                pygame.display.update()

        S.fill(colours.BACKGROUND)

        S.blit(TITLE, ((WIDTH - TITLE.get_width()) / 2, 50))
        for button in buttons:
            button.draw(S)

        pygame.display.update()
