def English(constraint):
    location = {"right": "to the right of",
                "above": "on top of",
                "left":  "to the left of",
                }[constraint.direction]

    return (f"Place a [{constraint.newcolour}] brick [{location}] a [{constraint.basecolour}] brick.", ("", "ttf"))


def Chinese(constraint):
    location = {"right": "的右边",
                "above": "上",
                "left":  "的左侧",
                }[constraint.direction]

    cols = {"white": "白",
              "red": "红",
              "yellow": "黄",
              "green": "绿",
              "blue": "蓝",
              "black": "黑",
              }

    return (f"将[{cols[constraint.newcolour]}]砖放在[{cols[constraint.basecolour]}]砖[{location}]。", ("CJKsc", "otf"))


def Hindi(constraint):
    location = {"right": "दाईं ओर",
                "above": "ऊपर",
                "left":  "बाईं ओर",
                }[constraint.direction]

    cols = {"white": "सफेद",
              "red": "लाल",
              "yellow": "पीला",
              "green": "हरा",
              "blue": "नीला",
              "black": "काली",
              }

    return (f"[{cols[constraint.newcolour]}] ईंट के [{cols[constraint.basecolour]}] ईंट के [{location}] रखें।", ("Devanagari", "ttf"))


def Spanish(constraint):
    location = {"right": "a la derecha de",
                "above": "sobre",
                "left":  "a la izquierda de",
                }[constraint.direction]

    cols = {"white": "blanco",
              "red": "rojo",
              "yellow": "amarillo",
              "green": "verde",
              "blue": "azul",
              "black": "negro",
              }

    return (f"Coloque un ladrillo [{cols[constraint.newcolour]}] [{location}] un ladrillo [{cols[constraint.basecolour]}].", ("", "ttf"))


def French(constraint):
    location = {"right": "à la droite d'",
                "above": "sur ",
                "left":  "à gauche d'",
                }[constraint.direction]

    cols = {"white": "blanche",
              "red": "rouge",
              "yellow": "jaune",
              "green": "verte",
              "blue": "bleue",
              "black": "noire",
              }

    return (f"Placez une brique [{cols[constraint.newcolour]}] [{location}]une brique [{cols[constraint.basecolour]}].", ("", "ttf"))


def Bengali(constraint):
    location = {"right": "ডানদিকে",
                "above": "উপরে",
                "left":  "বামদিকে",
                }[constraint.direction]

    cols = {"white": "সাদা",
              "red": "লাল",
              "yellow": "হলুদ",
              "green": "সবুজ",
              "blue": "নীল",
              "black": "কালো",
              }

    return (f"একটি [{cols[constraint.basecolour]}] ইটের  [{location}] একটি  [{cols[constraint.newcolour]}] ইট রাখুন।", ("Bengali", "ttf"))


def Portuguese(constraint):
    location = {"right": "à direita",
                "above": "em cima",
                "left":  "à esquerda",
                }[constraint.direction]

    cols = {"white": "branco",
              "red": "vermelho",
              "yellow": "amarelo",
              "green": "verde",
              "blue": "azul",
              "black": "preto",
              }

    return (f"Coloque um tijolo [{cols[constraint.newcolour]}] [{location}] de um tijolo [{cols[constraint.basecolour]}].", ("", "ttf"))


def German(constraint):
    location = {"right": "rechts von] einem",
                "above": "auf] einen",
                "left":  "links von] einem",
                }[constraint.direction]

    cols = {"white": "weißen",
              "red": "roten",
              "yellow": "gelben",
              "green": "grünen",
              "blue": "blauen",
              "black": "schwarzen",
              }

    return (f"Platziere einen [{cols[constraint.newcolour]}] Ziegelstein [{location} [{cols[constraint.basecolour]}] Ziegelstein.", ("", "ttf"))


def Italian(constraint):
    location = {"right": "a destra di",
                "above": "sopra",
                "left":  "a sinistra di",
                }[constraint.direction]

    cols = {"white": "bianco",
              "red": "rosso",
              "yellow": "giallo",
              "green": "verde",
              "blue": "blu",
              "black": "nero",
              }

    return (f"Posiziona un mattone [{cols[constraint.newcolour]}] [{location}] un mattone [{cols[constraint.basecolour]}].", ("", "ttf"))


def Russian(constraint):
    location = {"right": "справа от",
                "above": "сверху",
                "left":  "слева от",
                }[constraint.direction]

    cols = {"white": "белого",
              "red": "красный",
              "yellow": "желтого",
              "green": "зеленый",
              "blue": "синего",
              "black": "черный",
              }

    return (f"Поместите [{cols[constraint.newcolour]}] кирпич [{location}] [{cols[constraint.basecolour]}] кирпича.", ("", "ttf"))


languages = {"easy":   [English, Spanish, French, German, Italian, Portuguese],
             "medium": [English, Spanish, French, German, Russian, Chinese],
             "hard":   [English, Italian, Russian, Hindi, Chinese, Bengali]}

# Basic test of several random sentences
if __name__ == "__main__":
    import random
    from collections import namedtuple
    Constraint = namedtuple("Constraint", ["basecolour", "direction", "newcolour"])
    with open("colours.txt", "r") as f:
        COLOURS = f.read().split("\n")[:-1]
    for _ in range(20):
        c = Constraint(random.choice(COLOURS), random.choice(("above", "left", "right")), random.choice(COLOURS))
        print(English(c)[0].replace("[", "").replace("]", ""))
        print(random.choice(list(languages.values())[random.randrange(0, 3)])(c)[0].replace("[", "").replace("]", ""))
        print()
