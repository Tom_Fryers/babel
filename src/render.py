import pygame
from pygame.locals import *

from . import colours

pygame.init()

def render_line(words, font, sep=" "):
    """Render a single line of coloured text"""
    surfaces = []
    words[0][0].lstrip(" ")
    for i, word in enumerate(words):
        if i == 0 or word[1] != words[i - 1][1] or (i == 1 and words[0][0] == ""):
            thissep = ""
        else:
            thissep = sep

        if word[1]:
            surfaces.append(font.render(thissep + word[0], 1, colours.HIGHLIGHT))
        else:
            surfaces.append(font.render(thissep + word[0], 1, colours.FOREGROUND))

    final = pygame.Surface((sum(s.get_width() for s in surfaces), max(s.get_height() for s in surfaces)), SRCALPHA)
    x = 0
    for surface in surfaces:
        final.blit(surface, (x, 0))
        x += surface.get_width()

    return final


def size_line(words, font, sep=" "):
    """Determine the width of a line of text"""
    return font.size(sep.join(words))


def draw_text(text, font, difficulty=None, boxsize=(632, 512), fontsize=45):
    """Render some text to a Surface, with added line breaks"""
    font = pygame.font.Font(f"data/fonts/NotoSans{font[0]}-hinted/NotoSans{font[0]}-Regular.{font[1]}", fontsize)
    textbox = pygame.Surface(boxsize, SRCALPHA)
    textbox.convert_alpha()

    if difficulty == "hard":
        text = text.replace("[", "").replace("]", "")

    lines = [[]]
    for text in text.split("\n\n"):
        texts = [["", False]]
        for character in text:
            if character == "[":
                texts.append(["", True])
            elif character == "]":
                texts.append(["", False])
            else:
                texts[-1][0] += character

        if " " in text:
            mode = " "
            for section in texts:
                section[0] = section[0].split(" ")

        else:
            mode = ""


        for s, section in enumerate(texts):
            for w, word in enumerate(section[0]):
                relevant_text = [w[0] for w in lines[-1]] + [word]
                if w == len(section[0]) - 1 and s != len(texts) - 1 and word != '':
                    relevant_text += texts[s + 1][0][0]
                if size_line(relevant_text, font, mode)[0] < boxsize[0]:
                    lines[-1].append((word, section[1]))
                else:
                    lines.append([(word, section[1])])
        lines.append([])

    lines = lines[:-1]
    for i, line in enumerate(lines):
        textbox.blit(render_line(line, font, mode), (0, font.get_linesize() * i))
    return textbox
