BACKGROUND = (100, 150,  50)
FOREGROUND = (200, 180,  50)
HIGHLIGHT  = (217, 217, 122)
BRICKCOLOURS = {"white":  (255, 255, 255),
                "red":    (255,   0,   0),
                "yellow": (255, 255,   0),
                "green":  (  0, 255,   0),
                "blue":   (  0,   0, 255),
                "black":  (  0,   0,   0),
                }
TBCOLS = {"default": FOREGROUND,
          "hover": (180, 160, 30),
          "active": (160, 150, 20)}
