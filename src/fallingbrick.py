from . import drawbrick

class FallingBrick:
    def __init__(self, starty, endpos, image):
        self.y = starty
        self.coords = endpos
        self.image = image
        self.x = self.coords[0] * drawbrick.SIZE[0]
        self.maxpos = self.coords[1] * drawbrick.SIZE[1]
        self.velocity = 0
    
    def update(self, time):
        """Move the block down in accordance with gravity"""
        self.velocity += time * 9.80665 * 60  # pixels per metre
        self.y += self.velocity * time
        if self.y > self.maxpos:
            self.y = self.maxpos
            return True

    def draw(self, surface):
        surface.blit(self.image, (self.x, self.y))
