import pygame
from pygame.locals import *

from . import drawbrick, colours


class Button:
    def __init__(self, position, text):
        self.sound = pygame.mixer.Sound("data/sound/click.ogg")
        self.sound.set_volume(0.25)
        self.state = "default"
        self.text = text
        self.position = position
        self.update_image()

    def draw(self, surface):
        """Render the button to a surface"""
        surface.blit(self.image, self.position)

    def is_mouse_in(self, mouse_pos):
        """Check if a position is inside the button"""
        return self.position[0] < mouse_pos[0] < self.position[0] + self.image.get_width() and self.position[1] < mouse_pos[1] < self.position[1] + self.image.get_height()

    def mouse_over(self, mouse_pos):
        """Change state if the button is hovered over"""
        if self.state != "active":
            previous = self.state
            if self.is_mouse_in(mouse_pos):
                self.state = "hover"
            else:
                self.state = "default"

            if self.state != previous:
                self.update_image()


    def click(self, mouse_pos):
        """Activate upon being clicked"""
        if self.is_mouse_in(mouse_pos):
            self.state = "active"
            self.update_image()
            self.sound.play()

    def release(self, mouse_pos):
        """Return True if the button has been clicked"""
        if self.state == "active":
            self.state = "default"
            self.update_image()
            if self.is_mouse_in(mouse_pos):
                return True
        return False

class TextButton(Button):
    def __init__(self, text, font, colours, position):
        self.font = font
        self.colours = colours
        super().__init__(position, text)

    def update_image(self):
        self.image = self.font.render(self.text, 1, self.colours[self.state])


def highlight_box(image, state):
    drawsize = image.get_size()
    drawsize = (drawsize[0] - 1, drawsize[1] - 1)
    if state == "hover":
        pygame.draw.rect(image, colours.FOREGROUND, (0, 0) + drawsize, 2)
    elif state == "active":
        pygame.draw.rect(image, colours.FOREGROUND, (0, 0) + drawsize, 6)


class BrickButton(Button):
    def __init__(self, colour, position):
        self.colour = colour
        super().__init__(position, colour)

    def update_image(self):
        self.image = drawbrick.BRICKS[self.colour].copy()
        highlight_box(self.image, self.state)


class ColumnButton(Button):
    def __init__(self, size, position, column):
        self.size = size
        super().__init__(position, column)

    def update_image(self):
        self.image = pygame.Surface(self.size, SRCALPHA)
        self.image.convert_alpha()
        highlight_box(self.image, self.state)
