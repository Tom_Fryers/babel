import random
from collections import namedtuple

import pygame

from . import drawbrick, fallingbrick

Constraint = namedtuple("Constraint", ["basecolour", "direction", "newcolour"])

SIZE = (6, 8)
with open("src/colours.txt", "r") as f:
    COLOURS = f.read().split("\n")[:-1]
BRICKIMAGES = drawbrick.BRICKS


class Tower:
    def __init__(self, level):
        self.level = level
        self.bricks = [[self.pick_colour() if y == SIZE[1] - 1 else None for y in range(SIZE[1])] for x in range(SIZE[0])]

    def pick_colour(self):
        """Choose a random colour up to the tower's level"""
        return random.choice(COLOURS[:self.level + 1])

    def draw(self, fallingbricks):
        """Render the tower to a surface"""
        image = pygame.Surface((SIZE[0] * 64, SIZE[1] * 64), pygame.SRCALPHA)
        image.convert_alpha()

        for x, col in enumerate(self.bricks):
            for y, brick in enumerate(col):
                if brick is None or any((x, y) == b.coords for b in fallingbricks):
                    continue
                image.blit(BRICKIMAGES[brick], (x * 64, y * 64))
        return image

    def get_constraint(self):
        """Create a random constraint"""
        options = []
        for x in range(SIZE[0]):
            for y in range(SIZE[1] - 1):
                if self.bricks[x][y] is None and self.bricks[x][y + 1] is not None:
                    options.append(Constraint(self.bricks[x][y + 1], "above", self.pick_colour()))
                    if x > 0 and self.bricks[x - 1][y] is not None:
                        options.append(Constraint(self.bricks[x - 1][y], "right", self.pick_colour()))
                    if x < SIZE[0] - 1 and self.bricks[x + 1][y] is not None:
                        options.append(Constraint(self.bricks[x + 1][y], "left", self.pick_colour()))

        return random.choice(options)

    def get_top(self, column):
        """Find the top empty cell in a column"""
        for y in range(SIZE[1]):
            if self.bricks[column][y] is not None:
                return y - 1

    def check(self, constraint, colour, position):
        """Determine if a placement satisfies a constraint"""
        # Check if placed brick is the right colour
        if constraint.newcolour != colour:
            return False

        # Find the brick that the constraint depends on
        if constraint.direction == "above":
            relevantbrick = self.bricks[position[0]][position[1] + 1]
        elif constraint.direction == "right":
            if position[0] == 0:
                return False
            relevantbrick = self.bricks[position[0] - 1][position[1]]
        elif constraint.direction == "left":
            if position[0] == SIZE[0] - 1:
                return False
            relevantbrick = self.bricks[position[0] + 1][position[1]]

        # Check if the base brick is the correct colour
        if relevantbrick == constraint.basecolour:
            return True
        return False

    def add_brick(self, column, colour):
        """Add a brick to the top of a column"""
        self.bricks[column][self.get_top(column)] = colour

    def block_count(self):
        """Count how many blocks are in the tower"""
        return sum(int(self.bricks[x][y] is not None) for x in range(SIZE[0]) for y in range(SIZE[1]))

    def disassemble(self, fallingbricks):
        """Turn some bricks randomly into fallingbricks"""
        n = self.block_count()
        for x in range(SIZE[0]):
            for y in range(SIZE[1]):
                if self.bricks[x][y] is None:
                    continue
                done = False
                if random.random() < (0.1 + 0.02 * y) / n:
                    fallingbricks.append(fallingbrick.FallingBrick(y * drawbrick.SIZE[1], (x, SIZE[1]), BRICKIMAGES[self.bricks[x][y]]))
                    self.bricks[x][y] = None
        return n == 0
