import random

import pygame
from pygame.locals import *

from . import bricks, text, render, ui, drawbrick, fallingbrick, colours



def button_pos(column):
    """Get the draw position of a BrickButton"""
    return (484, round(82 + column * 89.6))


def initial_time(difficulty, level):
    """Get the amount of time to be given at the start of the game"""
    if difficulty == "easy":
        return 60 + level * 10
    elif difficulty == "medium":
        return 40 + level * 6
    elif difficulty == "hard":
        return 20 + level * 2
    else:
        raise ValueError('Difficulty must be "easy", "medium" or "hard"')


def extra_time(difficulty, level):
    """Get the amount of time to be added for each correct block"""
    if difficulty == "easy":
        return 5 + level * 0.5
    elif difficulty == "medium":
        return 4 + level * 0.5
    elif difficulty == "hard":
        return 3 + level * 0.5
    else:
        raise ValueError('Difficulty must be "easy", "medium" or "hard"')

def play(S, difficulty):
    WIDTH = S.get_width()
    HEIGHT = S.get_height()

    with open("src/colours.txt", "r") as f:
        COLOURS = f.read().split("\n")[:-1]

    clock = pygame.time.Clock()

    TOWERPOS = (50, 82)
    TEXTPOS = (598, 100)
    BACKCOL = colours.BACKGROUND

    TOWERSIZE = (drawbrick.SIZE[0] * bricks.SIZE[0], drawbrick.SIZE[1] * bricks.SIZE[1])

    FONTFL = pygame.font.Font("data/fonts/NotoSans-hinted/NotoSans-Regular.ttf", 100)
    FONTFS = pygame.font.Font("data/fonts/NotoSans-hinted/NotoSans-Regular.ttf", 70)
    FONTFI = pygame.font.Font("data/fonts/NotoSans-hinted/NotoSans-Regular.ttf", 30)

    BACKGROUND = pygame.image.load("data/background.png")
    HEART = pygame.image.load("data/heart.png")

    WRONGIM = FONTFI.render("Incorrect. Click to continue.", 1, (255, 0, 0))

    FPS = 60

    if difficulty == "easy":
        lives = 7
    elif difficulty == "medium":
        lives = 5
    elif difficulty == "hard":
        lives = 3

    thud = pygame.mixer.Sound("data/sound/thud.ogg")

    error = pygame.mixer.Sound("data/sound/error.ogg")
    error.set_volume(0.4)

    score = 0
    world = pygame.Surface(TOWERSIZE)
    wall = bricks.Tower(1)
    for level in range(1, 6):
        time = initial_time(difficulty, level)

        # Set up first constraint
        constraint = wall.get_constraint()
        constraint_text, lang = text.languages[difficulty][0](constraint)  # Always English
        constraint_image = render.draw_text(constraint_text, lang, difficulty)

        # Create buttons
        buttons = [ui.BrickButton(COLOURS[i], button_pos(i)) for i in range(level + 1)]
        buttons.append(ui.TextButton("Quit to Menu", FONTFI, colours.TBCOLS, (WIDTH - 230, 25)))
        columnsize = (drawbrick.SIZE[0], TOWERSIZE[1])
        colbuttons = [ui.ColumnButton(columnsize, (TOWERPOS[0] + column * drawbrick.SIZE[0], TOWERPOS[1]), column) for column in range(bricks.SIZE[0])]

        fallingbricks = []
        brickselected = None
        showold = False
        won = False
        lost = False
        winning = False
        losing = False
        while not (won or lost):
            mp = pygame.mouse.get_pos()
            for button in buttons:
                button.mouse_over(mp)
            if brickselected is not None:
                for button in colbuttons:
                    button.mouse_over(mp)
            action = None
            for event in pygame.event.get():
                if event.type == QUIT:
                    return "quit"

                elif event.type == MOUSEBUTTONDOWN:
                    if showold:
                        showold = False
                        continue
                    for button in buttons:
                        button.click(mp)

                    if brickselected is not None:
                        for button in colbuttons:
                            button.click(mp)

                elif event.type == MOUSEBUTTONUP:
                    for button in buttons:
                        if button.release(mp):
                            action = button.text

                    if brickselected is not None:
                        for button in colbuttons:
                            if button.release(mp):
                                # Work out final position
                                col = button.text
                                fp = wall.get_top(col)
                                pos = (col, fp)

                                # Place the new brick
                                fallingbricks.append(fallingbrick.FallingBrick(mp[1] - TOWERPOS[1] - drawbrick.SIZE[1] / 2, pos, bricks.BRICKIMAGES[brickselected]))
                                wall.add_brick(col, brickselected)

                                # Determine if the brick is valid
                                if wall.check(constraint, brickselected, pos):
                                    score += 1
                                    time += extra_time(difficulty, level)
                                    time = min(time, 118)
                                else:
                                    error.play()
                                    lives -= 1
                                    score -= 5
                                    score = max(score, 0)
                                    englishversion = render.draw_text(text.languages[difficulty][0](constraint)[0], ("", "ttf"), difficulty)
                                    oldconstraint = constraint_image
                                    showold = True
                                    if lives == 0:
                                        losing = 1
                                        break

                                # Move to the next level
                                if fp == 0:
                                    if level == 5:
                                        pygame.time.delay(200)
                                        won = True
                                    else:
                                        winning = True
                                        newwall = bricks.Tower(level + 1)
                                # Or generate a new constraint
                                else:
                                    constraint = wall.get_constraint()
                                    constraint_text, lang = random.choice(text.languages[difficulty][:level + 1])(constraint)
                                    constraint_image = render.draw_text(constraint_text, lang, difficulty)

                        # Deselect a brick if clicked anywhere
                        brickselected = None

                        # Deactivate colbuttons if they have not been clicked.
                        for button in colbuttons:
                            button.state = "default"
                            button.update_image()
            if action == "Quit to Menu":
                return None
            if action in COLOURS and not losing:
                brickselected = action

            # Update falling bricks
            l = len(fallingbricks)
            fallingbricks = list(filter(lambda fb: not fb.update(1 / FPS), fallingbricks))
            if len(fallingbricks) < l and not losing:
                thud.play()

            # Do losing animation
            if (losing and not fallingbricks) or losing > 1:
                losing = 2
                # Check if tower is fully broken
                if wall.disassemble(fallingbricks):
                    losing = 3

                # Stop if bricks have stopped falling
                if losing == 3 and not fallingbricks:
                    pygame.time.delay(200)
                    lost = True

            # Update time and check for time run out
            time -= 1 / FPS
            if time <= 0:
                pygame.time.delay(200)
                lost = True

            # Draw the brick area
            world.blit(BACKGROUND, (0, 0))
            if winning:
                winning = int(winning) + 3
                if winning > TOWERSIZE[-1]:
                    winning = TOWERSIZE[-1]
                    won = True
                world.blit(newwall.draw([]), (0, winning - TOWERSIZE[1]))

            world.blit(wall.draw(fallingbricks), (0, winning))

            for fb in fallingbricks:
                fb.draw(world)

            # Draw the rest of the screen
            S.fill(BACKCOL)

            S.blit(world, TOWERPOS)

            # Show constraint
            if showold:
                S.blit(oldconstraint, TEXTPOS)
                S.blit(englishversion, (TEXTPOS[0], TEXTPOS[1] + 300))
            else:
                S.blit(constraint_image, TEXTPOS)

            # Draw buttons
            if not showold:
                for button in buttons + colbuttons:
                    button.draw(S)

            # Draw selected rectangle around current brickbutton
            if brickselected is not None:
                buttonloc = button_pos(COLOURS.index(brickselected))
                pygame.draw.rect(S, colours.FOREGROUND, buttonloc + tuple(v - 1 for v in drawbrick.SIZE), 6)

            # Draw lives
            for life in range(lives):
                S.blit(HEART, (50 + life * 40, 25))

            # Draw time bar
            if time < 10 and time % 1 < 0.5:
                # Flashing
                pygame.draw.rect(S, (255, 0, 0), (50, 632, time * 10, 50))
                pygame.draw.rect(S, (255, 0, 0), (50, 632, 1180, 50), 3)
            else:
                pygame.draw.rect(S, colours.FOREGROUND, (50, 632, time * 10, 50))
                pygame.draw.rect(S, colours.FOREGROUND, (50, 632, 1180, 50), 3)

            # Draw score
            scoreim = FONTFI.render(f"Level {level}    Score: {score}", 1, colours.FOREGROUND)
            S.blit(scoreim, (600, 25))

            # Draw incorrect
            if showold:
                S.blit(WRONGIM, (600, 63))
            
            if brickselected is not None:
                S.blit(bricks.BRICKIMAGES[brickselected], (mp[0] - drawbrick.SIZE[0] / 2, mp[1] - drawbrick.SIZE[1] / 2))

            pygame.display.update()
            clock.tick(FPS)

        if lost:
            losetimer = 0
            while True:
                for event in pygame.event.get():
                    if event.type == QUIT:
                        return "quit"
                    elif event.type == MOUSEBUTTONUP and losetimer >= FPS * 1:
                        return "lost"

                lostim = FONTFL.render("You Lost!", 1, colours.FOREGROUND)
                scoreim = FONTFS.render(f"Score: {score}", 1, colours.FOREGROUND)
                S.fill(BACKCOL)
                S.blit(lostim, ((WIDTH - lostim.get_width()) / 2, 150))
                S.blit(scoreim, ((WIDTH - scoreim.get_width()) / 2, 400))
                pygame.display.update()
                clock.tick(FPS)
                losetimer += 1

        if won:
            score += 10
            wall = newwall
            lives += 1

    # Win screen
    if difficulty == "easy":
        score *= 2
    elif difficulty == "medium":
        score *= 3
    elif difficulty == "hard":
        score *= 4
    
    wintimer = 0
    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                return "quit"
            elif event.type == MOUSEBUTTONUP and wintimer >= FPS * 1:
                return "won"

        wonim = FONTFL.render("You Won!", 1, colours.FOREGROUND)
        scoreim = FONTFS.render(f"Score: {score}", 1, colours.FOREGROUND)
        S.fill(BACKCOL)
        S.blit(wonim, ((WIDTH - wonim.get_width()) / 2, 150))
        S.blit(scoreim, ((WIDTH - scoreim.get_width()) / 2, 400))
        pygame.display.update()
        clock.tick(FPS)
        wintimer += 1
