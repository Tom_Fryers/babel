import pygame

from . import colours

SIZE = (64, 64)
AA = 16

COLOURS = colours.BRICKCOLOURS

def generate_brick(colour):
    brick = pygame.image.load("data/brick.png")
    pygame.draw.circle(brick, COLOURS[colour], (AA * SIZE[0] // 2, AA * SIZE[1] // 2), round(SIZE[1] * AA * 0.3))
    return pygame.transform.smoothscale(brick, SIZE)

BRICKS = {colour: generate_brick(colour) for colour in COLOURS}
